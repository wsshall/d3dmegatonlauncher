﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace D3DMegatonLauncher
{
    /// <summary>
    /// Interaction logic for SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : Window
    {

        string pathToSteam;

        public SettingsDialog(string pathToSteam)
        {
            InitializeComponent();
            this.pathToSteam = pathToSteam;
            txtSteamPath.Text = pathToSteam;
        }

        private void BtnPathSubmit_Click(object sender, RoutedEventArgs e)
        {
            pathToSteam = txtSteamPath.Text;
            this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            txtSteamPath.Text = pathToSteam;
            txtSteamPath.SelectAll();
            txtSteamPath.Focus();
        }

        public string GetPathToSteam()
        {
            return pathToSteam;
        }
    }
}
