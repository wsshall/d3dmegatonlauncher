﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;

namespace D3DMegatonLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string PATH_FILE = "steampath.txt";
        private const string DEFAULT_PATH = "C:\\Program Files (x86)\\Steam\\Steam.exe";

        ProcessStartInfo start = new ProcessStartInfo(); // Handles launch parameters
        string pathToSteam; // Path to Steam executable

        public MainWindow()
        {
            InitializeComponent();

            if (File.Exists(PATH_FILE) == true)
                pathToSteam = File.ReadAllText(PATH_FILE);
            else
            {
                pathToSteam = DEFAULT_PATH;
                File.WriteAllText(PATH_FILE, DEFAULT_PATH);
                MessageBox.Show("Steam path not found. Using default path\nC:\\Program Files (x86)\\Steam\\Steam.exe");
            }

            start.FileName = pathToSteam;
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;

        }

        private void BtnDukeAtomic_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "-applaunch 225140 -nogamepad";

            LaunchGame(start);
        }

        private void BtnDukeCaribbean_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "-applaunch 225140 -addon 3 -nogamepad";

            LaunchGame(start);
        }

        private void BtnDukeDC_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "-applaunch 225140 -addon 1 -nogamepad";

            LaunchGame(start);
        }

        private void BtnDukeWinter_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "-applaunch 225140 -addon 2 -nogamepad";

            LaunchGame(start);
        }

        private void LaunchGame(ProcessStartInfo start)
        {
            try
            {
                using (Process proc = Process.Start(start))
                {
                    Application.Current.Shutdown();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            SettingsDialog settingsDialog = new SettingsDialog(pathToSteam);
            if (settingsDialog.ShowDialog() == true)
                pathToSteam = settingsDialog.GetPathToSteam();
            start.FileName = pathToSteam;
            File.WriteAllText(PATH_FILE, pathToSteam);
            MessageBox.Show("Path updated! (" + pathToSteam + ")");
        }
    }
}
